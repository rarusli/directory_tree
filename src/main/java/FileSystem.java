import beans.MyFile;

import java.util.Map;

public class FileSystem {

    private MyFile root;

    public FileSystem() {
        root = new MyFile();
    }

    /**
     * Create file for the given path
     *
     * @param path  Path to create.
     */
    public void createFile(String path) {
        if (path == null || path.trim().equals("") ) return;

        MyFile temp = root;
        String[] paths = path.split("/");

        for (int i=0; i<paths.length; i++) {

            Map<String,MyFile> listOfFiles =  temp.getFiles();

            if (!listOfFiles.containsKey(paths[i])) {
                listOfFiles.put(paths[i], new MyFile(paths[i]));
            }

            temp = listOfFiles.get(paths[i]);
        }
    }

    /**
     * Print current directory tree.
     */
    public void printDirectoryTree() {
        if (root == null)   return;
        printRecursive(root, 0);
    }

    /**
     * Helper method to help print directory tree recursively.
     *
     * @param file
     * @param numSpaces
     */
    private void printRecursive(MyFile file, int numSpaces) {
        if (file == null) return;

        Map<String,MyFile> listOfFiles = file.getFiles();

        for (Map.Entry<String,MyFile> entry : listOfFiles.entrySet()) {
            String filename = entry.getKey();
            MyFile subDirectory = entry.getValue();

            String spaces = numSpaces == 0 ? "" : String.format("%" + numSpaces + "s", " ");
            System.out.println(spaces + filename);

            printRecursive(subDirectory, numSpaces+2);
        }
    }

    /**
     * Delete file for the given filename.
     *
     * @param pathToDelete  Filename of file to delete.
     * @return Files that is deleted.
     */
    public MyFile deleteFile(String pathToDelete) {
        if (pathToDelete == null || pathToDelete.trim().equals("") ) return null;

        MyFile temp = root;
        String[] paths = pathToDelete.split("/");
        String filenameToDelete = paths[paths.length-1];

        MyFile fileDeleted = null;

        for (int i=0; i<paths.length; i++) {

            Map<String,MyFile> listOfFiles =  temp.getFiles();

            // if find correct path to delete
            if (i==paths.length-1 && listOfFiles.containsKey(filenameToDelete)) {
                fileDeleted = listOfFiles.get(filenameToDelete);
                listOfFiles.remove(filenameToDelete);
                break;
            }

            temp = listOfFiles.get(paths[i]);
            if (temp == null) {
                System.out.println("Cannot delete " + pathToDelete + " - " + paths[i] + " does not exist" );
                break; // path doesn't exist.
            }
        }

        return fileDeleted;
    }

    /**
     * Move file from source to destination.
     *
     * @param pathToSource  Source file.
     * @param pathToDestination Destination file.
     */
    public void moveFile(String pathToSource, String pathToDestination) {

        if (pathToSource == null || pathToSource.trim().equals("") ) return;
        if (pathToDestination == null || pathToDestination.trim().equals("") ) return;

        MyFile fileDeleted = deleteFile(pathToSource);

        MyFile temp = root;
        String[] paths = pathToDestination.split("/");

        for (int i=0; i<paths.length; i++) {

            Map<String,MyFile> listOfFiles =  temp.getFiles();

            if (i==paths.length-1 && listOfFiles.containsKey(paths[i])) {
                MyFile fileToAppendTo = listOfFiles.get(paths[i]);
                fileToAppendTo.getFiles().put(fileDeleted.getName(), fileDeleted);
            }

            temp = listOfFiles.get(paths[i]);
        }
    }

}