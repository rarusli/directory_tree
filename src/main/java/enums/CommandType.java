package enums;

public enum CommandType {

    CREATE("CREATE"),
    MOVE("MOVE"),
    DELETE("DELETE"),
    LIST("LIST");

    private String commandName;

    CommandType(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }

}