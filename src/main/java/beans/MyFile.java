package beans;

import java.util.Map;
import java.util.TreeMap;

/**
 * Representing a file.
 */
public class MyFile {

    /**
     * Name of the file.
     */
    private String name = "";

    /**
     * List of the file's subfile
     */
    private Map<String, MyFile> files;

    public MyFile() {
        name = "";
        files = new TreeMap<>();
    }

    public MyFile(String name) {
        this.name = name;
        files = new TreeMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, MyFile> getFiles() {
        return files;
    }

    public void setFiles(Map<String, MyFile> files) {
        this.files = files;
    }
}
