import enums.CommandType;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Runner class.
 */
public class FileSystemRunner {

    public static void main(String[] args) {
        FileSystemRunner fileSystemRunner = new FileSystemRunner();
        fileSystemRunner.run("input.txt");
    }

    private FileSystem fileSystem = new FileSystem();

    /**
     * Run the program.
     *
     * @param inputFileName
     */
    public void run(String inputFileName)  {
        String line;
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResourceAsStream(inputFileName)))) {
            while ((line = br.readLine()) != null) {
                processCommand(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Process command given the input.
     *
     * @param line  Line of input.
     * @throws Exception
     */
    private void processCommand(String line) throws Exception {
        System.out.println(line);
        String[] tokens = line.split("\\s{1,}");
        String command = tokens[0];
        if (command.equals(CommandType.CREATE.getCommandName())) {
            String fileToCreate = tokens[1];
            fileSystem.createFile(fileToCreate);
        } else if (command.equals(CommandType.LIST.getCommandName())) {
            fileSystem.printDirectoryTree();
        } else if (command.equals(CommandType.DELETE.getCommandName())) {
            String fileToDelete = tokens[1];
            fileSystem.deleteFile(fileToDelete);
        } else if (command.equals(CommandType.MOVE.getCommandName())) {
            fileSystem.moveFile(tokens[1], tokens[2]);
        }
    }
}
